---
title: Información importante de LifeCLEF - BirdCLEF para el proyecto "Identificación de Aves"
date: 2021-11-13
publish: True
tags: [birdclef, lifeclef, xenocanto, audio]
author: Andrea-Ortega
license: ccbysa
katex: True
--- 


### Introducción

LifeCLEF - BirdCLEF es una competencia anual que consiste en desarrollar un identificador de aves por especie a partir de grabaciones de cantos de aves tomadas en el sur de América. Este desafío se ha realizado desde el 2014 y Xeno-canto, que es una organización social internacional de ornitólogos aficionados y expertos, colaboró con la base de datos.  Los primeros dos años, las grabaciones contenían un pájaro cantor dominante en primer plano y algunos otros en el fondo, después se agregaron grabaciones que no apuntan a una especie específica y que pueden contener un número arbitrario de pájaros cantores. Se pueden hacer comparaciones interesantes, por su similitud, con otros desafíos como: ICML4B 2013 y NIPS4B 2013.

En el laboratorio L52+ del IIMAS se está realizando un proyecto similar a las competencias mencionadas; se han tomado los datos de entrenamiento del BirdCLEF 2017, por lo que en adelante la información mostrada se limita a dicho evento. 

### Sobre los datos de entrenamiento

En BirdCLEF 2017, el conjunto de datos de entrenamiento fue de 36,496 grabaciones pertenecientes a 15,000 especies de aves, tomadas en la unión de Brasil, Colombia, Venezuela, Guyana, Surinam, Guayana Francesa, Bolivia, Ecuador y Perú. 

Los registros de audio están asociados a varios metadatos como el tipo de sonido (llamada, canción, alarma, vuelo, etc.), la fecha y localización de las observaciones, algunos comentarios textuales de los autores, nombres comunes multilingües y calificaciones de calidad colaborativas. Los metadatos disponibles para cada grabación incluyen:

- MediaId: el ID del registro de audio
- FileName: el nombre del archivo del registro de audio (normalizado)
- ClassId: el ID de clase que debe usarse como verdad fundamental
- Especie: el nombre de la especie
- Género: el nombre del género, un nivel por encima de la especie en la jerarquía taxonómica utilizada por Xeno-canto
- Familia: el nombre de la familia, dos niveles por encima de la especie en la jerarquía taxonómica utilizada por Xeno-canto
- Subespecie: (si está disponible) el nombre de la subespecie, una nivel bajo la especie en la jerarquía taxonómica utilizada por Xeno-canto
- VernacularNames: (si está disponible) nombre(s) común(es) en inglés
- Antecedentes Especies: nombres en latín (especies de género) de otras especies audibles eventualmente mencionadas por el registrador
- Fecha: (si está disponible) la fecha en que se observó el ave
- Hora: (si está disponible) la hora en que se observó el ave
- Calidad: Varía de 1 a 5, siendo 1 la mejor calidad y 5 la peor calidad, 0 significa calificación desconocida.
- Localidad: (si está disponible) el nombre de la localidad, la mayoría de las veces una ciudad
- Latitud y longitud
- Elevación: altitud en metros
- Autor: nombre del autor del registro
- AuthorID: id del autor del registro
- Contenido de audio: lista separada por comas de tipos de sonido como 'llamada' o 'canción', de forma libre
- Comentarios: comentarios de los grabadores

El campo "fondo" en los metadatos puede indicar si hay algunas otras especies identificadas en el fondo. Es posible que algunos registros de audio tampoco contengan en absoluto una especie de ave dominante.
El conjunto de datos de entrenamiento contiene solo registros de audio con una especie de ave dominante, con o sin otras especies de aves identificadas en el fondo.

## Detalles en el registro de audio

Para evitar cualquier sesgo en la evaluación relacionada con los dispositivos de grabación utilizados, la Univ normalizó todos los datos de audio. Equipo Toulon Dyni: normalización de la muestra de ancho de banda / frecuencia a 44,1 kHz, formato .wav (16 bits).  También proporcionan funciones de audio para grabaciones de entrenamiento y prueba basadas en las funciones de coeficientes cepstrales de Mel Filter optimizadas para llamadas de aves. 
Se calcularon 16 MFCC (primer coeficiente = energía logarítmica) en ventanas de 11,6 ms, cada 3,9 ms. Derivaron la velocidad y aceleración, dando como resultado una línea de 16*3 características por cuadro. Más detalles de ésto en: http://sabiod.org/nips4b/






