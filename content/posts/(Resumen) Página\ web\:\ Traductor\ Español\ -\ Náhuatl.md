---
title: "(Resumen) Página web: Traductor Español – Náhuatl" 
date: 2021-10-29
publish: True
tags: [náhuatl, traducción, webapp, lenguas originarias ]
author: Alfredo Sánchez
author_link: https://www.linkedin.com/in/alfredo-s%C3%A1nchez-995374215 
license: ccbysa
katex: True
--- 


El desarrollo del sitio web fue a partir de NodeJs con la implementación de módulos que faciliten su manejo como lo es express como frame work que ayuda a la escritura de código, EJS como motor de plantillas para mayor utilización de HTML si es que se llegase a utilizar, y Morgan para poder ver qué es lo que se está pidiendo en tiempo real y poder ver las peticiones de los registros mientras se está ejecutando los archivos recién guardados sin necesidad de hacerlo manal.La instalación de los módulos se realiza desde una terminal dentro de la carpeta raíz del proyecto.

Gran parte también fue la utilización de Bootstrap como aplicación multiplataforma que es un conjunto de herramientas que tiene también base en CSS. Para hacer uso de esta aplicación se debe de implementar las librerías necesarias como encabezado del mismo código o como head que redireccione a una ruta que lo contenga si se requiere más orden.

El material gráfico que acompaña el proyecto fue obtenido de
[freepik.com](http://freepik.com) como material de uso libre. Los íconos de la UNAM y del IIMAS que redirigen a sus portales respectivos fueron tomados de los mismos sitios.

Link:

[https://github.com/AlfredoSanc/interfazTraductor](https://github.com/AlfredoSanc/interfazTraductor)
