---
name: "Javier Álvarez"
img: https://secure.gravatar.com/avatar/0f7ad8ce1d97e6614ce5381d07a6990a
date: 2021-02-01
role: 
    es: "Investigador externo"
    en: "External researcher"
active: true
now: 
---

Soy doctor en física por la UNAM, pero actualmente me encuentro laborando en la industria como desarrollador. 

Entre mis intereses de investigación se encuentra el reconocimiento de hablantes por medio de redes neuronales. 
