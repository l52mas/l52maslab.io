---
name: "Alejandro Serralde"
img: "https://robohash.org/Alejandro Serralde ?set=set4"
date: 2024-02-29
role: 
    es: "Servicio Social"
    en: "Social service"
active: true
now: 
---

Estudiante de actuaria de la Facultad de Ciencias interesado en aspectos de
programación y estadística. Y participación en proyecto de desarrollo de
sistemas. 


