---
name: "Ivan Meza"
img: https://s.gravatar.com/avatar/095f372c5a12726df8a222679070149f?s=80
twitter: ivanvladimir
date: 2020-01-01
role: 
    es: "Coordinador del laboratorio"
    en: "Laboratory coordinator"
active: true
now: 
---

Soy investigador en el 
[IIMAS](http://www.iimas.unam.mx)/[UNAM](http://www.unam.mx) trabajando en el 
[Departamento de Ciencias de la Computación](http://turing.iimas.unam.mx).

Mis principal interés de investigación es la intersección entre el lenguaje 
(humano) y el cómputo. Me concentro en [aprendizaje 
automático](https://en.wikipedia.org/wiki/Machine_learning), [inteligencia 
artificial](https://en.wikipedia.org/wiki/Artificial_intelligence) y 
[procesamiento del lenguaje 
natural](https://en.wikipedia.org/wiki/Natural_language_processing)

Aquí pueden encontrar más sobre
[mi](https://turing.iimas.unam.mx/~ivanvladimir/).

Además imparto el curso de [Lenguajes Formales y
Autómatas](http://turing.iimas.unam.mx/~ivanvladimir/page/curso_lfya/) en la
[Facultad de Ingeniería](https://www.ingenieria.unam.mx/).
