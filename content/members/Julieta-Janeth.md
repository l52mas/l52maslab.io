---
name: "Julieta Janeth"
img: https://s.gravatar.com/avatar/df57ce6bba1de6625ac5295ae0c94271?s=80
date: 2021-06-19
role: 
    es: "Servicio Social"
    en: "Social service"
active: true
now: 
---

Actualmente soy estudiante de física interesada en la ciencia de datos y deep learning. Me encuentro trabajando el servicio social con el Dr. Iván Meza en el proyecto de *"identificación de canto de aves"*.

En este blog me interesa publicar tutoriales respecto al uso de paqueterías así como contenido de divulgación de la ciencia. Si deseas saber más al respecto dejo mis páginas de divulgación como de arte en instagram, *@sembrandolaureles* y *@chuliieta*, respectivamente.
