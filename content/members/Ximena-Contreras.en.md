---
name: "Ximena Contreras"
img: ""
date: 2022-10-16 
role: 
    es: "Proyecto de Tesis"
    en: "Thesis project."
active: true
#now: 
---

I study in the Faculty of Sciences, the Mathematics career. I'm interested in Deep Learning Models and Data Analysis.
