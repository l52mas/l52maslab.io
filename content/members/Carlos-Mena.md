---
name: "Carlos Mena"
img: http://1.gravatar.com/userimage/230924433/de0c375cd85e95f916de247c316828cd
twitter: Un_Pato
date: 2021-01-02
role: 
    es: "Colaborador"
    en: "Colaborator"
active: true
now: 
---


[Investigador postdoctoral](https://www.ru.is/haskolinn/starfsfolk/carlosm) en la [Universidad de Reikiavik](https://en.ru.is/).
Especialista en Reconocimiento de Voz para lenguas de pocos recursos, incluidas
variantes del español como la Mexicana.
