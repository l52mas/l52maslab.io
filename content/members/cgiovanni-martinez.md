---
name: "Carlos Giovanni"
img: https://secure.gravatar.com/avatar/c47fd7077ed961977542306737b737a2
twitter: cgiovvnni
date: 2021-09-13
role: 
    es: "Servicio Social"
    en: "Social service"
active: true
now: 
---

Estudiante de Ingeniería en Computación de la Facultad de Ingeniería de la UNAM.
Mis principales interese son la Inteligencia Artificial, Deep Learning, Data
Science y Rocket Science.

Actualmente estoy colaborando en el proyecto *Imágenes Astronómicas* en el que
se busca identificar características importantes de colecciones de fotografías
de galaxias.
