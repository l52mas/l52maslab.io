---
title: "Normalización de textos"
date: 2019-02-09T23:18:21-06:00
draft: false
status: active
code: normalization
---

Hoy en día existen grandes colecciones de textos que con fácilmente accesibles en línea. Sin embargo estos textos muchas veces no están en los formatos adecuados. En esta área se desarrollan proyectos que toman clases de estos documentos como leyes, sentencias, informes, etc. Y generamos herramientas que los hacen más accesibles para su análisis. Algunos de las colecciones que se han trabajado son:

* Las sentencias de la Corte Interamericana de Derechos Humanos

