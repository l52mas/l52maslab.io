---
title: "Sistemas conversacionales"
date: 2019-02-09T17:41:23-06:00
draft: false
code: chatvoice
---

En esta área exploramos como crear sistemas inteligentes que puedan tener una conversacion. Recientemente este tipo de sistemas se han popularizado con tecnología como Siri, Google Assitant o Alexa; sin embargo en el laboratorio queremos expandir las fronteras y capacidades de estos sistemas a través del estudio de los siguientes aspectos de una conversación:

* Memoria consistente a través del tiempo
* Conversaciones entre agentes con objetivos contrapuestos
* Reacción sentimental


