---
title: "Reconocimiento y machine learning para voz"
date: 2019-02-09T23:22:35-06:00
draft: false
status: active
code:
---

Aunque hoy en día paraciera que el reconocimiento de voz es un tema superado, aun existen grandes retos para estos como es el reconocimiento con multiple hablantes, con modificaciones del habla o distancias largas. 

Otro problema que esta línea de investigación trata de superar es tener una autonomía del reconocimeinto de voz. Actualmente existen muchos servicios que permiten reconocer el español de méxico, sin embargo los sistemas no son directamente accessibles y es imposible realizar investigación sin acceso a estos servicios. Por lo anterior, en esta línea de investigación se busca recolectar grandes colecciones de audio que sean de libre uso.

