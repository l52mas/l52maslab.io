---
title: "Aprendiendo del Internet"
date: 2019-02-09T22:57:54-06:00
draft: false
status: active
code: ilearning
---

Es innegable que en el Internet existen grandes cantidades de información, sin embargo, toda esta riqueza de conocimiento rara vez es utilizada para que la computadora adquiera conocimiento de ella. En esta línea de investigación estamos interesados en desarrollar metodologías que a una computadora le permitan explotar documentos de Internet. 

En particular, en los últimos años hemos desarrollado métodos para aprender a navegar resultados de búsquedas. En los siguientes años esperamos extender estos mecanismos a extracción de información usando aprendizaje por refuerzo. 
