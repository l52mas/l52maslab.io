---
name: "ENC: Traducción automática para lenguas originarias de México"
date: 2021-09-04
happening: 2021-09-04
status: finish
---

## Contexto

[Tutorial](http://computo.fismat.umich.mx/smcc/index.php/enc/enc-2021/videos-tutoriales)
impartido como parte del [Encuentro Nacional de Cómputo
2021](http://computo.fismat.umich.mx/smcc/) organizado por la [Sociedad Mexicana
de Ciencias de la Computación](https://ceniem.umich.mx/smcc/)

## Objetivo

Presentar el estado del arte en la traducción de lenguajes originarios de México
y proporcional código.

## Ponentes

* [Delfino Zacarías]({{< ref "/members/delfino-zacarías.md" >}})
* [Ivan Meza]({{< ref "/members/ivan-meza.md" >}})



## Sesiones

* [Video 1](https://www.youtube.com/watch?v=OeHs8fdpUg0)
* [Video 2](https://youtu.be/lNbLWevlyaU)

## Material de apoyo

* [Presentación](https://docs.google.com/presentation/d/1hMOeQw5NLGQK6CtN7CL478RrNdJ6px0rqxHDEDLknUg/edit?usp=sharing)
* [Carpeta](https://drive.google.com/drive/folders/16r-g4Ah5sDDEnU8y_YlGUpJU6K9XpTmH?usp=sharing)
