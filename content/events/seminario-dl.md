---
name: "Seminario DL"
date: 2022-09-02
happening: 2022-08-02
status: finish
---

El seminario de DL es un seguimiento que se hace al material de clase DS-GA
1008 que se imparte en NYU Center for Data Science.

Para saber más de la dinámica haz click [aquí](https://l52mas.gitlab.io/seminariodl/).

