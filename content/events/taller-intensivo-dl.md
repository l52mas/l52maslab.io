---
name: "Taller Intensivo DL"
date: 2020-09-17
happening: 2020-09-17
status: finish
---

## Objetivo

Revisar los conceptos principales de las redes profundas y sus fundamentos matemático-computacionales.

## Temario
| Día  hora       | Tema                                                  | Material                                                                                                                                                                                                                                                                        | Tipo     |
| --------------- | ----------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------- |
| Jueves, 10hrs   | Las matemáticas del *deep learning*                   | [📽️](https://docs.google.com/presentation/d/1ngfn_ywbdG7Z3CSqyV-CE60RQlo60Sw_1Iq2S2WbVas/edit?usp=sharing)                                                                                                                                                                     | Teoría   |
| Jueves, 11hrs   | Una red neuronal fully connected en pytorch           | [📽️](https://docs.google.com/presentation/d/1OfN1A12fE6atVAX7OiRYu9VHTZgIIgoH-8M1LPhHHU4/edit?usp=sharing)[📓](https://colab.research.google.com/drive/1hm5Sxv4I6zxPlMtUvrZ1sMN2EGVOF_UV?usp=sharing)                                                                          | Práctica |
| Jueves, 12hrs   | La cuadratura de la circularidad de los  datos        | [📽️](https://docs.google.com/presentation/d/1jkthYo_ZNw_dO5FlSbzy1F7e7JZ8Czba77xj6nIgFQ4/edit?usp=sharing)                                                                                                                                                                     | Teoría   |
| Jueves, 13hrs   | Cómo entrenar a tu dragón (NN)                        | [📓](https://colab.research.google.com/drive/13TiOpojmwugv38nHkYoDqQ0cXr6mPgHP?usp=sharing)                                                                                                                                                                                     | Pŕactica |
| Jueves, 16hrs   | Redes convolucionales                                 | [📽️](https://docs.google.com/presentation/d/15Mkxx9fnD5-gBki7_0pl46AW965bO8XqAnr2Z7-_X7A/edit?usp=sharing)                                                                                                                                                                     | Teoría   |
| Jueves, 17hrs   | Clasificación de imágenes                             | [📓](https://colab.research.google.com/drive/1vnu_e0Nv2ZsNrEb17S7Upkgxc2MBd3VY?usp=sharing)                                                                                                                                                                                     | Práctica |
| Jueves, 18hrs   | Espacios esotéricos y arquitecturas no convencionales | [📽️](https://docs.google.com/presentation/d/1y5F1SS4RURMRTwd6prHxZFdY37avVw7ncV4ww9HM2oM/edit?usp=sharing)                                                                                                                                                                     | Teoría   |
| Viernes, 10hrs  | Transformers                                          | [📽️](https://docs.google.com/presentation/d/1b9jwPtougPM2eddZ8dRbXYJA5mYe03xNm8CIkFk4k9o/edit?usp=sharing)                                                                                                                                                                     | Teoría   |
| Viernes, 11hrs  | Clasificadores de texto                               | [📓](https://colab.research.google.com/github/jalammar/jalammar.github.io/blob/master/notebooks/bert/A_Visual_Notebook_to_Using_BERT_for_the_First_Time.ipynb#scrollTo=To9ENLU90WGl)[📓](https://colab.research.google.com/drive/1pPyaqbM3DO6svRExFocv_IGq1_nLIoX_?usp=sharing) | Práctica |
| Viernes, 13hros | Problemas éticos del campo                            | [📽️](https://docs.google.com/presentation/d/1DA9NT37FRls5oO_DOlhfNqmY3abKu_lxzp3ihF071us/edit?usp=sharing)                                                                                                                                                                     | Teoría   |


## Videos

* [Sesión 1](https://www.youtube.com/watch?v=mBiSyI9mqd4)
* [Sesión 2](https://www.youtube.com/watch?v=U9evVg9zAkg)


# Temario y links

## Las matemáticas de Deep Learning

**Temas**

- Multiplicación de matrices
- Vectores vs matrices
- Tensores
- Funciones continuas
- Distribución probabilistas

**Links para conocer más**

- [Introduction to Linear Algebra (sitio web)](https://math.mit.edu/~gs/linearalgebra/) [en]
- [Mathematics for Machine Learning (sitio web)](https://mml-book.github.io/) [en]
- [Mathematics for Machine Learning (pdf)](https://mml-book.github.io/book/mml-book.pdf) [en]
- [Machine learning cheat sheet (pdf)](https://github.com/soulmachine/machine-learning-cheat-sheet/raw/master/machine-learning-cheat-sheet.pdf) [en]
## Una red fully connected en pytorch

**Arquitectura de una red fully connected**



**Links para conocer más**

- [Hacker's guide to Neural Networks (blogpost)](https://karpathy.github.io/neuralnets/) [en]
- [Hacker's guide to Neural Networks (colab)](https://colab.research.google.com/drive/1m0lNJ9n8LUXHHU4pOLrZZjSMjoeRKCrN?usp=sharing#scrollTo=Wo4RCtsPuC4n) [en]
- [Descenso por gradiente (Gradient descent) (post)](https://turing.iimas.unam.mx/~ivanvladimir/posts/gradient_descent/) [es]
- [PyTorch 101, Part 1: Understanding Graphs, Automatic Differentiation and Autograd (post)](https://blog.paperspace.com/pytorch-101-understanding-graphs-and-automatic-differentiation/)  [en]


## La cuadratura de la circularidad de los  datos

**La razón de ser de ML**

**Descenso por gradiente**

**Links para conocer más**

- [Dataset search](https://datasetsearch.research.google.com/) (search engine) [en]
- [Fashion-MNIST](https://github.com/zalandoresearch/fashion-mnist) (dataset) [en]
- [PyTorch 101, Part 1: Understanding Graphs, Automatic Differentiation and Autograd (post)](https://blog.paperspace.com/pytorch-101-understanding-graphs-and-automatic-differentiation/)  [en]


## Cómo entrenar a tu dragón (NN)

**División del dataset**

**Pasos de entrenamiento**

**Funciones auxiliares en pytorch**

**Links para conocer más**

- [Train / Dev / Test sets (video lectura)](https://www.coursera.org/lecture/deep-neural-network/train-dev-test-sets-cxG1s) [en]
- [Train, Test, & Validation Sets explained (post)](https://deeplizard.com/learn/video/Zi-0rlM4RDs) [en]


## Redes convolucionales

**Convolución**

**Tres capas una red**

**Links para conocer más**

- [Lecture 2: Convolutions (videlo lecture)](https://computationalthinking.mit.edu/Fall20/lecture2/) [en]
- [Convolutional Neural Networks (CNNs / ConvNets) (notas curso)](https://cs231n.github.io/convolutional-networks/) [en]


## Espacios esotéricos y arquitecturas no convencionales

**Espacios multidimensionales**

Arquitecturas EBM

**Links para conocer más**

- [Visualizing what ConvNets learn (notas curso)](https://cs231n.github.io/understanding-cnn/) [en]
- [First steps in PyTorch : classifying fashion objects (FashionMNIST) (notas curso)](https://teaching.pages.centralesupelec.fr/deeplearning-lectures-build/00-pytorch-fashionMnist.html) [en]
## Transformers

**Atención**

**Transformación**

**Links para conocer más**
[](https://lilianweng.github.io/lil-log/2017/10/15/learning-word-embedding.html)
- [Learning Word Embedding](https://lilianweng.github.io/lil-log/2017/10/15/learning-word-embedding.html) (post) [en]
- [Attention Is All You Need](https://arxiv.org/abs/1706.03762) (artículo) [en]
- Awesome Bert Nl (list of links) [en]
- [The Annotated Transformer (notas)](http://nlp.seas.harvard.edu/2018/04/03/attention.html) [en]
- [The Illustrated Transformer (post)](https://jalammar.github.io/illustrated-transformer/) [en]
- [Visualizing Bert Embeddings (post)](https://krishansubudhi.github.io/deeplearning/2020/08/27/bert-embeddings-visualization.html) [en]
- [Transformers from scratch (post)](http://peterbloem.nl/blog/transformers) [en]
## Problemas éticos de las redes neuronales

**Links para conocer más**

- [Practical Data Ethics (curso)](https://ethics.fast.ai/) [en]


