---
name: "1er Encuentro del Laboratorio L52+"
date: 2024-01-17
happening: 2024-01-24
status: finish
---

Este será el primer encuentro que organizaremos entre miembros del laboratorio.
Donde tendremos conversatorios con miembros pasados, cursos, una sesión de
carteles y una plática magistral. El programa es el siguiente:

<div class="notification is-warning">
Agradecemos el apoyo del personal de servicio y técnico del IIMAS que hicieron
posible la realización del evento y que nos permitieron estar de forma cómoda en
las instalaciones.
</div>


### Miércoles 24 de enero

<table class="table">
  <thead>
      <tr>
        <th style="width:15%"><abbr title="Hora">Horario</abbr></th>
        <th style="width:15%"><abbr title="Tipo">Tipo</abbr></th>
        <th><abbr title="Act.">Actividad</abbr></th>
      </tr>
  </thead>
  <tbody>
    <tr>
        <td>
            De 16 a 18:45 horas
        </td>
        <td>
            <span class="tag is-info">Curso</span>
        </td>
        <td>
            <strong>Fundamentos teóricos del aprendizaje profundo</strong></br>
            Imparte: Ivan Meza, IIMAS, UNAM</br>
            Temario:
	<ol>
    <li>Introducción
	<li>Redes neuronales</li>
    <li><em>Back propagation</em></li>
    <li>Evaluación</li>
	</ol>    <div class="buttons">
        <button class="js-modal-trigger button is-primary is-size-7" data-target="curso-fundamentos">
              Ver detalle de temario
            </button></br>
            <a
            href="https://docs.google.com/presentation/d/e/2PACX-1vTtoRuBtLt1UNIMf6PJCH_YImEqwRiAGnMZDUCiiVw3mQgRjq3kiIPd7dU1Aa8RGNkRqcUngXVdLbFS/pub?start=false&loop=false&delayms=3000"
            class="button is-link is-size-7" target="_blank">Material</a>
            </div>
        </td>
    </tr>
   </tbody>
</table>


### Jueves 25 de enero

<table class="table">
  <thead>
      <tr>
        <th style="width:15%"><abbr title="Hora">Horario</abbr></th>
        <th style="width:15%"><abbr title="Tipo">Tipo</abbr></th>
        <th><abbr title="Act.">Actividades</abbr></th>
      </tr>
  </thead>
  <tbody>
    <tr>
        <td>
            De 10 a 11:15 horas
        </td>
        <td>
            <span class="tag is-success">Conversatorio</span>
        </td>
        <td>
            <strong>Trabajando en el extranjero</strong></br>
            Modera: Carlos Saúl Rivera Landeros, Facultad de Ciencias, UNAM</br>
            Participan:
            <ul>
                <li>Flor Chacón, Microsoft. Redmond, Washington, EUA</li>
                <li>Teo Solís, Oracle</li>
                <li>Héctor Ricardo Murrieta Bello, Novo Nordisk, Dinamarca</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            De 11:30 a 12:45 horas
        </td>
        <td>
            <span class="tag is-success">Conversatorio</span>
        </td>
        <td>
            <strong>Estudiando en el extranjero</strong></br>
            Modera: Héctor Becerril</br>
            Participan:
            <ul>
                <li>Mateo Torres Ruiz, University College London, Londres, UK</li>
                <li>Albert Manuel Orozco Camacho, Concordia University / Mila -
                Québec AI Institute. Montréal, Canadá</li>
                <li>Alonso Palomino, DFKI, Alemania</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            De 13:00 a 14:15 horas
        </td>
        <td>
            <span class="tag is-success">Conversatorio</span>
        </td>
        <td>
            <strong>Escribiendo la Tesis</strong></br>
            Modera: Jorge García Flores, CNRS - Université Sorbonne Paris Nord - Francia</br>
            Participan:
            <ul>
                <li>Mayra Cid Oros, Bosonit, Ciudad de México </li>
                <li>Yunuen Sarasuadi Acosta, Lingüísta ENAH-Desarroladora DEV.F </li>
                <li>Liber Adrián Hernández Abad, Accenture</li>
                <li>Ricardo Garcia Garcia, Bluetab an IBM Company</li>
                <li>Emmanuel Maqueda, IIMAS, UNAM / IBM México CDMX</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            De 16:00 a 17:15 horas
        </td>
        <td>
            <span class="tag is-info">Curso</span>
        </td>
        <td>
            <strong>Interactuando con ChatGPT y otros (Prompt Engineering)</strong></br>
            Imparte: Oscar René Garzón Castro, Facultad de Ciencias, UNAM, CDMX,
            México</br>
            Temario:
            <ol><li>Introducción</li>
                <li>Técnicas de Prompt Engineering y ejemplos</li>
                <li>Para seguir aprendiendo</li>
            </ol>
            <div class="buttons">
            <button class="js-modal-trigger button is-primary is-size-7" data-target="curso-interactuando">
              Ver detalle de temario
            </button></br>
            <a
            href="https://www.canva.com/design/DAF67Kh19lw/lzlm6iJzPy26Bb5ebBKFsw/view"
            class="button is-link is-size-7" target="_blank">Material</a>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            De 17:30 a 19:00 horas
        </td>
        <td>
            <span class="tag is-info">Curso</span>
        </td>
        <td>
            <strong>Programando tu primer clasificador de textos</strong></br>
            Imparte: David A. Duran Ruiz, UNAM, Ciudad de México, México </br>
            Temario:
            <ol><li>Introducción</li>
                <li>Datos</li>
                <li>Fine-tuning</li>
                <li>Publicación del modelo</li>
                <li>Preguntas</li>
            </ol>
            <div class="buttons">
            <button class="js-modal-trigger button is-primary is-size-7" data-target="curso-programando">
              Ver detalle de temario
            </button></br>
            <a
            href="https://colab.research.google.com/drive/1DQvy8yYTV40cljOOZjqJxOF1JEHg9Jka?usp=sharing"
            class="button is-link is-size-7" target="_blank">Material</a>
            </div>
        </td>
    </tr>
   </tbody>
</table>

### Viernes 26 de enero

<table class="table">
  <thead>
      <tr>
        <th style="width:15%"><abbr title="Hora">Horario</abbr></th>
        <th style="width:15%"><abbr title="Tipo">Tipo</abbr></th>
        <th><abbr title="Act.">Actividades</abbr></th>
      </tr>
  </thead>
  <tbody>
    <tr>
        <td>
            De 10 a 11:15 horas
        </td>
        <td>
            <span class="tag is-success">Conversatorio</span>
        </td>
        <td>
            <strong>Trabajando en la industria México</strong></br>
            Modera: Ximena Contreras, Facultad de Ciencias, UNAM. C.U. México.</br>
            Participan:
            <ul>
                <li>Axel Castillo Sánchez, Chamberlain Group. Nogales, Sonora,
                México.</li>
                <li>Ricardo Garcia Garcia, Bluetab an IBM Company</li>
                <li>Sandra Vázquez, BBVA /  Academia: CCG UNAM</li>
                <li>Karina Flores, BBVA</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            De 11:30 a 12:45 horas
        </td>
        <td>
            <span class="tag is-warning">Cárteles</span>
        </td>
        <td>
            <strong>Sesión de carteles</strong></br>
            Presentan</br>
			<small>
 			<button class="js-modal-trigger button is-primary is-size-7" data-target="lista-carteles">
              Ver lista de carteles
            </button></small></br>
        </td>
    </tr>
    <tr>
        <td>
            De 13:00 a 14:00 horas
        </td>
        <td>
            <span class="tag is-danger">Plática magistral</span>
        </td>
        <td>
            <strong>Aproximando la diversidad lingüística desde una perspectiva
            computacional</strong></br>
            Ponente: Dra. Ximena Gutiérrez, CEIICH-UNAM, México</br>
            Modera: Dra. Cecilia Reyes Peña, Universidad Politécnica Metropolitana de Hidalgo, Tolcayuca, México e IIMAS, UNAN, México</br>
            <div class="buttons">
            <button class="js-modal-trigger button is-primary is-size-7" data-target="platica-magistral-resumen">
              Ver resumen de plática
            </button></br>
            <a
            href="https://direct.mit.edu/coli/article/doi/10.1162/coli_a_00489/116635"
            class="button is-link is-size-7" target="_blank">Artículo asociado</a>
            </div>
        </td>
    </tr>
   </tbody>
</table>

<div id="platica-magistral-resumen" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
	<h2>Aproximando la diversidad lingüística desde una perspectiva computacional</h2>
	<h3>Dra. Ximena Gutiérrez, CEIICH-UNAM, México</h3>
	En esta presentación platicaré sobre mi incursión en el área de
	Procesamiento de lenguaje natural. Particularmente, elaboraré sobre
	la importancia de modelar la gran diversidad inherente a los
	lenguajes naturales cuando desarrollamos tecnologías del lenguaje.
	Más allá de la generación de tecnología, los enfoques
	computacionales pueden ser una valiosa herramienta para contestar
	preguntas de investigación interdisciplinarias, así como
	complementar el estudio del lenguaje humano.
		</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>

<div id="curso-programando" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
	<h2>Programando tu primer clasificador de textos</h2>
	<h3>David A. Duran Ruiz, UNAM, Ciudad de México, México</h3>
	<ol>
    <li>Introducción
		<ul>
        <li>¿Qué es huggingface?</li>
        <li>¿Qué es un modelo pre-entrenado?</li>
        <li>¿Por qué usar modelos pre-entrenados?</li>
        <li>Ejemplos de modelos comunes</li>
        <li>¿Qué es fine-tuning?</li>
		</ul>
	</li>
	<li>Datos<ul>
        <li>Etiquetado de datos</li>
        <li>Preprocesamiento (limpieza y tokenización)</li>
		</ul>
	</li>
    <li>Fine-tuning<ul>
        <li>Split del dataset: training, validation y test</li>
        <li>Entrenamiento</li>
        <li>Evaluación</li>
	</ul></li>
    <li>Publicación del modelo</li>
    <li>Preguntas</li>
	</ol>
		</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>

<div id="curso-fundamentos" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
	<h2>Fundamentos teóricos del aprendizaje profundo</h2>
	<h3>Ivan Meza, IIMAS, UNAM</h3>
	<ol>
    <li>Introducción
		<ul>
	    <li>Álgebra vectorial</li>
	    <li>Álgebra tensorial</li>
        <li>Aprendizaje supervisado</li>
		</ul>
	</li>
	<li>Redes neuronales<ul>
	    <li>Redes neuronales poco profundas</li>
	    <li>Redes neuronales profundas</li>
		</ul>
	</li>
    <li><em>Back propagation</em><ul>
        <li>Descenso por gradiente</li>
        <li>Funciones de perdida</li>
        <li>Grafos de operaciones</li>
        <li>Algoritmos de optimización </li>
        </ul>
    <li><em>Evaluación</em></li>
	</ol>
		</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>

<div id="lista-carteles" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
	<h2>Lista de carteles</h2>
Peronto
	<ul>
<li>Análisis de la aprobación a los candidatos en las elecciones 2023 en
Coahuila y el Estado de México a través de un modelo de lenguaje, Julieta Melina
Flores Morán y Santiago Pedro González López</li>
<li>Detección de ironía en textos, Gilberto Barranco Sánchez</li>
<li>Traducción Automática, Jesús Priego Morales</li>
<li>Modelación de personalidad usando prompt engineering, Oscar René Garzón Castro</li>
<li>Automatizando la descarga y conversión de videos, Ruy Cabello Cuahutle</li>
<li>Verificación de identidad en Chatvoice, Carlos Saúl Rivera Landeros</li>
<li>Lista de sístemas de síntesis por medio de IA, Cabrera Peralta César Alejandro</li>
<li>Interfaz Web App: Traductor de Lenguas Originarias de México, Mónica Jazmín García Sarabia</li>
<li>Optimización de POS Tagging mediante Modelos de Aprendizaje Profundo, Daniel Bautista Hernández</li>
<li>Biblioteca OpenNMT para modelos NLP, Jose Ramon Rangel Campos</li>
<li>Análisis de Sentimiento para Datos Turísticos, Luis A. Araiza Bautista</li>
<li>Corpus de YouTube, Aarón Santiago</li>
	</ul>
		</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>



<div id="curso-interactuando" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
	<h2>Interactuando con ChatGPT y otros (Prompt Engineering)</h2>
	<h3>Oscar René Garzón Castro, Facultad de Ciencias, UNAM, CDMX, México</h3>
	<ol>
    <li>Introducción
		<ul>
        <li>Contexto de LLM</li>
	    <li>Mencionar los más populares</li>
    	<li>Donde se pueden utilizar</li>
		<li>Breve explicación de su configuración</li>
    	<li>Breve explicación de cómo funcionan ??</li>
		</ul>
	</li>
	<li>Técnicas de Prompt Engineering y ejemplos:<ul>
        <li>Zero-shot</li>
	    <li>Few-shot</li>
    	<li>Chain-of-Thought</li>
    	<li>Self-Consistency</li>
    	<li>Role playing</li>
		</ul>
	</li>
    <li>Para seguir aprendiendo:<ul>
        <li>Links a papers interesantes</li>
        <li>Prompt engineering en generadores de imagen y video</li>
	</ul></li>
	</ol>
		</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>



<script>
document.addEventListener('DOMContentLoaded', () => {
  // Functions to open and close a modal
  function openModal($el) {
    $el.classList.add('is-active');
  }

  function closeModal($el) {
    $el.classList.remove('is-active');
  }

  function closeAllModals() {
    (document.querySelectorAll('.modal') || []).forEach(($modal) => {
      closeModal($modal);
    });
  }

  // Add a click event on buttons to open a specific modal
  (document.querySelectorAll('.js-modal-trigger') || []).forEach(($trigger) => {
    const modal = $trigger.dataset.target;
    const $target = document.getElementById(modal);

    $trigger.addEventListener('click', () => {
      openModal($target);
    });
  });

  // Add a click event on various child elements to close the parent modal
  (document.querySelectorAll('.modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button') || []).forEach(($close) => {
    const $target = $close.closest('.modal');

    $close.addEventListener('click', () => {
      closeModal($target);
    });
  });

  // Add a keyboard event to close all modals
  document.addEventListener('keydown', (event) => {
    if(e.key === "Escape") {
      closeAllModals();
    }
  });
});
</script>
