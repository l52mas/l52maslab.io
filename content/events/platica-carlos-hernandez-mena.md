---
name: "Planes de las Tecnologías de Lenguaje en Europa: Experiencias de primera mano desde el Reconocimiento Automático de Voz"
date: 2024-03-12
happening: 2024-04-05
status: finish
---

## Fecha y lugar

* **Fecha**: 5 de abril, 13 horas
* **Presencial**: Por enviar después de registro
* **Transmisión en línea**:  https://www.youtube.com/@laboratoriol52mas

## Resumen

En el mundo moderno, las sociedades humanas comienzan una nueva transformación dirigida hacia la creación de tecnologías de lenguaje tales como el procesamiento del lenguaje  natural, la traducción automática, el reconocimiento automático de voz y la síntesis del habla. En esta charla se hablará de algunas de las estrategias que diversos países de Europa han seguido para llevar a cabo esa transformación para sus lenguas oficiales y no oficiales, se expondrán algunos de los retos que han tenido que enfrentar en cuanto a infraestructura y se hará énfasis en cuán importante es el papel que los nativos de esas lenguas juegan en la recolección y verificación de datos de voz y texto.

## Ponente

* [Dr. Carlos Daniel Hernández Mena](https://www.bsc.es/hernandez-mena-carlos-daniel)</br>
  Research Engineering</br>
  [Barcelona Supercomputing Center](https://www.bsc.es/)</br>


## Material


<iframe width="560" height="315"
src="https://www.youtube.com/embed/UdIPjZPxcUY?si=SaEF6c_TYoY3-xS4&amp;start=1427"
title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;
clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>



* [Presentación](https://drive.google.com/file/d/1OrfAYUEiPTElwDlIt291x09zmbv3O9Cb/view?usp=sharing)

## Registro

[**Aquí**](https://forms.gle/chcyAzYutG8X7Z5U8)


## Contacto  

[**ivanvladimir+seminario@gmail.com**](mailto:ivanvladimir+seminario@gmail.com)


