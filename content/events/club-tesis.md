---
name: "Club de Tesis"
date: 2050-03-31
happening: 2050-03-31
status: draft
---

En el club de tesis le damos seguimiento a la escritura de las tesis que el
laboratorio apoya. 

Si estás haciendo una tesis en el laboratorio y te gustaría integrarte a las
actividades puedes escribir un correo a <a
href="mailto:ivanvladimir+club-tesis@gmail.com">Ivan Meza</a> para que te
agregue a las actividades.

