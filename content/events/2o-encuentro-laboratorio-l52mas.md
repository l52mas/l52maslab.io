---
name: "2er Encuentro del Laboratorio L52+"
date: 2024-12-12
happening: 2025-01-27
status: active
---

Este es el 2o encuentro que organizaremos entre miembros del laboratorio. Donde tendremos tutoriales, pláticas, sesión de carteles, conversatorio y pláticas magistrales. Las sesiones son virtuales e hibridas. 

<details>
  <summary><strong>Índice</strong></summary>
<ul>
<li><a href="#el-programa">El programa</a>
      <ul>
        <li><a href="#lunes">Lunes</a>
        <li><a href="#martes">Martes</a>
        <li><a href="#miercoles">Miércoles</a>
        <li><a href="#jueves">Jueves</a>
        <li><a href="#viernes">Viernes</a>
      </ul>
</li>
<li><a href="#modalidades">Modalidades</a>
</li>
<li><a href="#comité-organizador">Comité organizador</a>
</li>
</ul>
</details>

## El programa

<table class="table">
  <thead>
      <tr>
        <th style="width:15%;text-align:center;"><abbr title="Hora">Día</abbr></th>
        <th style="width:10%;text-align:center;"><abbr title="Hora">Horario</abbr></th>
        <th style="width:10%;text-align:center;"><abbr title="Modalidad">Modalidad</abbr></th>
        <th style="width:10%;text-align:center;"><abbr title="Tipo">Tipo</abbr></th>
        <th style="text-align:center;"><abbr title="Act.">Actividad</abbr></th>
      </tr>
  </thead>
  <tbody>
    <tr id="lunes">
        <td rowspan="3"><strong><s>Lunes 27 de enero</s></strong></td>
        <td bgcolor="Cornsilk">
            <em>12:00 a 14:00</em>
        </td>
        <td bgcolor="Cornsilk">
            Virtual
        </td>
        <td bgcolor="Cornsilk">
            <span class="tag is-info">Plática magistral</span>
        </td>
        <td bgcolor="Cornsilk">
            <strong>Construyendo una IA para las lenguas indígenas del continente Americano: ¿Qué podemos aprender de su morfología?</strong></br>
            <em>Dr. Manuel Mager, Amazon NY.</em>
            <div class="buttons">
            <button class="js-modal-trigger button is-primary is-size-7" data-target="platica-construyendo">
            Más información
            </button>
            </div>
        </td>
    </tr>
    <tr bgcolor="Cornsilk">
        <td>
            16:00 a 17:00
        </td>
        <td>
            Virtual
        </td>
        <td>
            <span class="tag is-info">Taller</span>
        </td>
        <td>
            <strong>Teoría de la computación</strong></br>
            <em>Luis Escobar, Facultad de Ciencias, IIMAS</em>
            <div class="buttons">
            <button class="js-modal-trigger button is-primary is-size-7" data-target="taller-teoria">
            Más información
            </button>
            </div>
        </td>
    </tr>
    <tr bgcolor="Cornsilk">
        <td>
            17:00 a 18:00
        </td>
        <td>
            Virtual
        </td>
        <td>
            <span class="tag is-info">Por definirse</span>
        </td>
        <td>
            <strong>Introducción JS</strong></br>
            <em>Yunuen Acosta, Broxia</em>
            <div class="buttons">
            <button class="js-modal-trigger button is-primary is-size-7" data-target="taller-js">
            Más información
            </button>
            </div>
        </td>
    </tr>
    <tr id='marted'>
        <td rowspan="6"><strong><s>Martes 28 de enero</s></strong></td>
        <td bgcolor="Cornsilk">
            12:00 a 12:30
        </td>
        <td bgcolor="Cornsilk">
            Virtual
        </td>
        <td bgcolor="Cornsilk">
            <span class="tag is-info">Conversatorio</span>
        </td>
        <td bgcolor="Cornsilk">
           <strong>LISAM: Traductor de Lengua de Señas Mexicana en Tiempo Real</strong></br>
            <em>Erik Dominguez, Yael Badillo, Efrén Hernández, Aldo Santiago, LISAM-SIAFI-FI</em>
            <div class="buttons">
            <button class="js-modal-trigger button is-primary is-size-7" data-target="conversatorio-lisam">
            Más información
            </button>
            </div>
        </td>
    </tr>
   <tr bgcolor="Cornsilk">
        <td>
            12:30 a 13:00
        </td>
        <td>
            Virtual
        </td>
        <td>
            <span class="tag is-info">Conversatorio</span>
        </td>
        <td>
            <strong>Análisis de textos argumentativos de alumnos de secundaria y preparatoria</strong></br>
            <em>Fermin Cristobal Perez, PCIC</em>
            <div class="buttons">
            <button class="js-modal-trigger button is-primary is-size-7" data-target="conversatorio-analisis">
            Más información
            </button>
            </div>
        </td>
    </tr>
    <tr bgcolor="Cornsilk">
        <td>
            13:00 a 13:30
        </td>
        <td>
            Virtual
        </td>
        <td>
            <span class="tag is-info">Conversatorio</span>
        </td>
        <td>
            <strong>Detección de cantos de especies animales: aves, peces y monos</strong></br>
            <em>Natalia Godinez, FC y Victor Gonzalez, FI</em>
            <div class="buttons">
              <button class="js-modal-trigger button is-primary is-size-7" data-target="proyecto-deteccion">
              Más información
              </button>
            </div>
        </td>
    </tr>
    <tr bgcolor="Cornsilk">
        <td>
            13:30 a 14:00
        </td>
        <td>
            Virtual
        </td>
        <td>
            <span class="tag is-info">Conversatorio</span>
        </td>
        <td>
            <strong>Distilling Visual Information into Symbolic Representations through Self-Supervised Learning</strong></br>
            <em>Victor Martinez, PCIC</em>
            <div class="buttons">
            <button class="js-modal-trigger button is-primary is-size-7" data-target="conversatorio-distilling">
            Más información
            </button>
            </div>
        </td>
   </tr>
   <tr bgcolor="#DAF7A6">
        <td>
            16:00 a 17:30
        </td>
        <td>
          Híbrido 
        </td>
        <td>
            <span class="tag is-info">Taller</span>
        </td>
        <td>
            <strong>Construyendo chatbots con LangChain</strong><br/>
            <em>David Duran, Facultad de Psicología, IIMAS</em>
            <div class="buttons">
            <button class="js-modal-trigger button is-primary is-size-7" data-target="platica-langchain">
            Más información
            </button>
            </div>
        </td>
    </tr>
    <tr bgcolor="#DAF7A6">
        <td>
            17:30 a 18:00
        </td>
        <td>
          Híbrido 
        </td>
        <td>
            <span class="tag is-info">Trivia</span>
        </td>
        <td>
            <strong>Concurso de trivia</strong></br>
            <div class="buttons">
            <button class="js-modal-trigger button is-primary is-size-7" data-target="trivia">
            Más información
            </button>
            </div>
        </td>
    </tr>
    <tr id='miercoles'>
        <td rowspan="4"><strong><s>Miércoles 29  de enero</s></strong></td>
        <td bgcolor="Cornsilk">
            12:00 a 13:00
        </td>
        <td bgcolor="Cornsilk">
            Virtual
        </td>
        <td bgcolor="Cornsilk">
            <span class="tag is-info">Conversatorio</span>
        </td>
        <td bgcolor="Cornsilk">
            <strong>[En inglés] AI in research and industry</strong></br>
            <em>Elise Garnett, Miguel Williams and Mosab Rezaei Chamberlain Group</em>
            <div class="buttons">
            <button class="js-modal-trigger button is-primary is-size-7" data-target="talk-chamberlain">
            Más información
            </button>
            </div>
        </td>
    </tr>
    <tr bgcolor="Cornsilk">
        <td>
            13:00 a 14:00
        </td>
        <td>
            Virtual
        </td>
        <td>
            <span class="tag is-info">Taller</span>
        </td>
        <td>
            <strong>El intraemprendedor moderno: El trébol de cuatro hojas en la era empresarial</strong></br>
            <em>Rogelio Meneses, Founder & CEO Keex Makerlab</em>
            <div class="buttons">
            <button class="js-modal-trigger button is-primary is-size-7" data-target="platica-infra">
            Más información
            </button>
            </div>
        </td>
    </tr>
    <tr bgcolor="#DAF7A6">
        <td>
            16:00 a 17:00
        </td>
        <td>
           Híbrido 
        </td>
        <td>
            <span class="tag is-info">Taller</span>
        </td>
        <td>
            <strong>Reducción de ruido y filtrado inteligente con Wavelets</strong></br>
            <em>Ximena Contreras, FC, Broxia</em>
            <div class="buttons">
            <button class="js-modal-trigger button is-primary is-size-7" data-target="platica-reduccion">
            Más información
            </button>
            </div>
        </td>
    </tr>
    <tr bgcolor="#DAF7A6">
        <td>
            17:00 a 18:00
        </td>
        <td>
           Híbrido 
        </td>
        <td>
            <span class="tag is-info">Taller</span>
        </td>
        <td>
            <strong>Modelos Ocultos de Márkov</strong></br>
            <em>Ramón Campos, FC</em>
            <div class="buttons">
            <button class="js-modal-trigger button is-primary is-size-7" data-target="taller-modelos">
            Más información
            </button>
            </div>
        </td>
    </tr>
    <tr id="jueves">
        <td rowspan="3"><strong><s>Jueves 30 de enero</s></strong></td>
        <td bgcolor="Cornsilk">
            12:00 a 13:00
        </td>
        <td bgcolor="Cornsilk">
            Virtual
        </td>
        <td bgcolor="Cornsilk">
            <span class="tag is-info">Taller</span>
        </td>
        <td bgcolor="Cornsilk">
            <strong>Temás básicos de Redes Neuronales</strong></br>
            <em>José Olmedo</em>
            <div class="buttons">
            <button class="js-modal-trigger button is-primary is-size-7" data-target="taller-rn">
            Más información
            </button>
            </div>
        </td>
    </tr>
    <tr bgcolor="Cornsilk">
        <td>
            13:00 a 14:00
        </td>
        <td>
            Virtual
        </td>
        <td>
            <span class="tag is-info">Taller</span>
        </td>
        <td>
            <strong>Redactando mi CV para la industria.</strong></br>
            <em>Axel Castillo Sánchez, Grupo Chamberlain</em>
            <div class="buttons">
            <button class="js-modal-trigger button is-primary is-size-7" data-target="taller-cv">
            Más información
            </button>
            </div>
        </td>
    </tr>
    <tr  bgcolor="PowderBlue">
        <td>
            16:00 a 18:00
        </td>
        <td>
            Presencial
        </td>
        <td>
            <span class="tag is-info">Taller magistral</span>
        </td>
        <td>
            <strong>Trustworthy AI: el marco Europeo para el desarrollo responsable de IA</strong></br>
            <em>Dra. Sofia Trejo</em></br>
            <div class="buttons">
            <button class="js-modal-trigger button is-primary is-size-7" data-target="taller-trustworthy">
            Más información
            </button>
            </div>
        </td>
    </tr>
    <tr id="viernes">
        <td rowspan="2"><strong>Viernes 31 de enero</strong></td> 
        <td bgcolor="PowderBlue" >
            10:00 a 12:00
        </td>
        <td bgcolor="PowderBlue" >
            Presencial
        </td>
        <td bgcolor="PowderBlue" >
            <span class="tag is-info">Sesión de carteles</span>
        </td>
        <td bgcolor="PowderBlue" >
           <button class="js-modal-trigger button is-primary is-size-7" data-target="poster-list">
            Lista de cartéles
            </button>
        </td>
    </tr>
    <tr bgcolor="PowderBlue">
        <td>
            12:00 a 14:00
        </td>
        <td>
           Presencial 
        </td>
        <td>
            <span class="tag is-info">Plática magistral</span>
        </td>
        <td>
            <strong>Genómica y Computación: Aplicaciones en biomedicina</strong></br>
            <em>Dra. Alejandra Cervera, Instituto Nacional de Medicina Genómica</em>
            <div class="buttons">
            <button class="js-modal-trigger button is-primary is-size-7" data-target="platica-genomica">
            Más información
            </button>
            </div>
        </td>
    </tr>
   </tbody>
</table>


## Modalidades

* <span style="color:Cornsilk">■</span>Virtual
* <span style="color:#DAF7A6">■</span>Híbrido, virtual y presencial 
* <span style="color:PowderBlue">■</span>Presencial


## Comité organizador

* Yunuen Acosta, Broxia
* Luis Mario Escobar, FC
* Natalia Godinez, FC
* Lizeth Palacios, FFyL
* José Olmedo, FI
* Ivan Meza, IIMAS
* Axel Castillo, Chamberlain Group

<div id="taller-teoria" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
  <h2>Teoría de la computación</h2></br>
  <h3>Luis Escobar, Facultad de Ciencias, IIMAS</h3>
  <p>El taller se enfocará en los fundamentos de la teoría de la computación, abarcando autómatas y lenguajes formales. A través de conceptos clave como máquinas de estado finito, gramáticas formales y la jerarquía de Chomsky. Se estudiará su estructura , importancia y utilidades prácticas en el desarrollo de software.
  </p>
  <h3>Semblanza</h3>
  <p>Egresado de la Facultad de Ciencias UNAM,participante en el Laboratorio L52+ IIMAS.Interesado en ramas de la computación como Teoría de Automatas  y Lenguajes Formales ,Teoría de la complejidad, Programación Funcional, etc.</p>
		</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>

<div id="poster-list" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
  <h2>Lista de carteles</h2></br>
  <ul>
  <li>
          <strong>Autómata Lineal con Frontera</strong><br/>
          <p>Luis Mario Escobar Rosales, FC-UNAM</p>
  </li>
  <li>
          <strong>Red de "Influencias" en tesis</strong><br/>
          <p>Dereck Miguel, FC-UNAM</p>
  </li>
  <li>
          <strong>Medición y modificación de la personalidad de chatbots con el uso de técnicas de prompteo</strong><br/>
          <p>Israel Varona, FP-UNAM</p>
  </li>
  <li>
          <strong>Identificación de voz forense</strong><br/>
          <p>Carlos Castaññón, FP-UNAM y Antonio Toledo, FI-UNAM</p>
  </li>
  <li>
          <strong>Distilling Visual Information into Symbolic Representations through Self-Supervised Learning </strong><br/>
          <p>Víctor Martinez, PCIC-UNAM</p>
  </li>
  </ul>
		</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>



<div id="platica-construyendo" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
	<h2>Construyendo una IA para las lenguas indígenas del continente Americano: ¿Qué podemos aprender de su morfología?</h2>
	<h3>Dr. Manuel Mager, Amazon NY</h3>
  <h3>Semblanza</h3>
  <p>
Manuel Mager es actualmente Científico Aplicado (Applied Scientist) en Amazon Web Services AI Labs (AWS AI Labs), en NYC, EE.UU. Estudió la licenciatura en Informática en la UNAM, la maestría en ciencias de la computación en la UAM Azcapotzalco  y el doctorado en el Instituto de Procesamiento Natural en la Universidad de Stuttgart, Alemania. Durante este tiempo, su investigación se ha enfocado en la aplicación de modelos neuronales para procesar lenguas indígenas del continente americano, generación de texto a partir de grafos, modelos de lenguaje y AI responsable . Actualmente trabaja en el desarrollo de AWS Bedrock. Sus trabajos han sido consistentemente publicados en conferencias de alto nivel de PLN, como ACL, NAACL, EMNLP, EACL y COLING, además de revistas indexadas. También ha sido parte del equipo fundador y organizador del workshop "PLN para Lenguas Indígenas del Continente Americano" (AmericasNLP) (2021-2025 y lo que falta!). Ha sido participante invitado al panel magistral en ACL 2022, y conferencia magistral para el WSDM Day 2024 y PLNIndigenas 2022, además de ser invitado para dar cátedras invitadas en universidades como la New York University (NYU), la University of Pittsburgh, entre otras.
</p>
		</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>

<div id="taller-js" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
	<h2>Introducción a JavaScript</h2>
	<h3>Yunuen Acosta, Broxia</h3>
  <p>
¿Te gustaría aprender a programar pero no sabes por dónde empezar? ¡Este taller es para ti! No necesitas experiencia previa. Te enseñaremos desde los conceptos básicos hasta cómo comenzar a armar tus propios proyectos, por ejemplo un minichatbot. Solo necesitas tener nociones de HTML y CSS, y muchas ganas de aprender</P>

<h4>Objetivos del Taller</h4>
<ol>
<li>
        Comprender los fundamentos de JavaScript.
        </li>
<li>
Aprender a utilizar Visual Studio Code para escribir y ejecutar código.
        </li>
<li>
Crear proyectos básicos utilizando JavaScript.
        </li>
<li>
Fomentar el interés y la confianza en la programación.
        </li>
</ol>

<h4>Requisitos</h4>
      <p>Muchas ganas de aprender.</p>
  </p>
  <h3>Semblanza</h3>
  <p>
        <em>En microsoft</em> Ha contribuido en la App Store de Microsoft al desarrollo de
videojuegos utilizando Javascript para el desarrollo de Immune de
Jikuri Interactive.inc<br/>
        <em>En Broxia</em> Ha ayudado en proyectos de IA para la industria energética mexicana
desarrollando interfaces que ofrecen un elevado estándar en la
consulta de datos masivos en tiempo real con ingeniería UX.
</p><p>
Reconocida por su destacada contribución en lingüística y tecnología,
Yunuen ha recibido una mención honorífica de la Escuela Nacional de
Antropología e Historia(ENAH), ha sido ponente en en el Coloquio de
Lingüística Computacional de la UNAM, en el Instituto de
Investigaciones de Lingüística Aplicada (CILA) de la Universidad
Nacional Mayor de San Marcos en el tema “Las bases de datos en la
identificación forense por voz”.Actualmente es parte del Laboratorio
+52, lidera un club de lectura feminista en el Fondo de Cultura
Económica, dónde se discuten obras que abordan temas de género y
empoderamiento, fomentando un espacio de reflexión y crecimiento.
  </p>
		</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>



<div id="talk-chamberlain" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
    <h2>AI in research and industry</h2><br/>
  <h3>Elise Garnett, Chamberlain Group</h3>
  <p>
    I will be discussing my work at Chamberlain Group as well as the difference between industry and academic research. I studied Computer Science at Northern Illinois University, with an emphasis in Machine Learning. I work at Chamberlain Group as a software/machine learning engineer. I used to run the Association for Computing Machinery at NIU and a cool fact about me is that I once worked on an AI project to help detect defeces in airplane parts for Boeing.
</p>
  <h3>Miguel Williams, NIU</h3>
  <p>
    My talk will be over using AI to identify similarities and differences of short conference paper and long conference papers utilizing sylometric features. I studied at Computer Science at Northern Illinois University (NIU). I am currently an instructor and Ph.D. student at NIU researching Text style transfer.
</p>
  <h3>Mosab Rezaei, NIU</h3>
  <p>My name is Mosab Rezaei, a PhD Student and teaching assistant in the computer science department at NIU (Northern Illinois University), and my current research area is stylometry. I am going to talk about generating texts wirh different authors' writing styles.</p>
		</div>
	</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>


<div id="proyecto-deteccion" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
    <h2>Detección de cantos de especies animales: aves, peces y monos</h2><br/>
  <h3>Natalia Godinez, FC y Victor Gonzalez, FI</h3>
  <p>
    El proyecto plantea el uso de redes neuronales para monitorear sonidos de fauna en México, promoviendo la conservación y el análisis de ecosistemas en ambientes terrestres y acuáticos.
</p>
  <h3>Semblanza</h3>
  <p>
    <em>Natalia Godinez</em>:  Egresada de Biología por la Facultad de Ciencias de la UNAM. Actualmente se encuentra formándose en temas de programación, ciencia de datos y bioética. Participa en el proyecto "Sonidos naturales en oídos artificiales", enfocado en el uso de redes neuronales para el monitoreo acústico de la biodiversidad. <br/>
    <em>Víctor Gonzélez</em>: Estudiante de último semestre de Ingeniería Eléctrica Electrónica, de la Facultad de Ingeniería de la UNAM. Con intereses en diferentes temas de TI, especialmente en  intelegencia artificial como técnicas de Deep Learning, Machine learning, etcétera. Actualmente becario en el proyecto Alianza UNAM-Huawei 2023 "Sonidos naturales en oídos artificiales". 
</p>
	</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>


<div id="conversatorio-lisam" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
    <h2>LISAM: Traductor de Lengua de Señas Mexicana en Tiempo Real</h2><br/>
    <h3>Erik Dominguez, Yael Badillo, Efrén Hernández, Aldo Santiago, LISAM-SIAFI-FI</h3>
  <p>
Desarrollo de un sistema basado en inteligencia artificial que utiliza redes neuronales para traducir en tiempo real la Lengua de Señas Mexicana a lenguaje natural, promoviendo la inclusión y la accesibilidad.
  </p>
  <h3>Semblanza</h3>
  <p>
Estudiantes de la Facultad de Ingeniería y miembros de SIAFI, la Sociedad de Inteligencia Artificial, comprometidos con la innovación tecnológica con responsabilidad social.
</p>
		</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>

<div id="conversatorio-distilling" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
    <h2>Distilling Visual Information into Symbolic Representations through Self-Supervised Learning</h2></br>
    <h3>Victor Martinez, PCIC</h3>
  <p>
El proyecto busca imitar la manera en que el ser humano es capaz de representar todas las variaciones de nuestro entorno por medio del lenguaje, por lo que se intenta hacer uso del SSL para que los modelos por si solos modelen y generen descripciones sin ningún tipo de guía humana.
  </p>
  <h3>Semblanza</h3>
<p>
Se considera un pensador fuera de la caja, estudie ingeniería en la FI, posteriormente trabaje en áreas relacionadas a IA/ML en Mexico y actualmente me encuentro cursando la maestría en el PCIC,
</p>
		</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>

<div id="conversatorio-analisis" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
    <h2>Análisis de textos argumentativos de alumnos de secundaria y preparatoria</h2></br>
    <h3>Fermin Cristobal Perez, PCIC</h3>
  <p>
Uso de modelos transformer generativos y discriminativos para la tarea de etiquetado de tokens en textos argumentativos producidos por estudiantes de secundaria y preparatoria.
  </p>
  <h3>Semblanza</h3>
<p>
Licenciado en Ciencias Físico-Matemáticas en la UMSNH. Actual estudiante de último semestre en el Posgrado en Ciencia e Ingeniería de la Computación en la UNAM
</p>
		</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>



<div id="platica-reduccion" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
	<h2>Reducción de ruido y filtrado inteligente con Wavelets</h2>
	<h3>Ximena Contreras, FC, Broxia</h3>
  <p>
Este taller enseñará una breve introducción a Transformada de Wavelet utilizando la paquetería PyWavelet para implementarlo con Python a una señal con ruido y realizar un filtrado con imagenes.
  </p>
  <h3>Semblanza</h3>
  <p>
Ximena Contreras estudió Matemáticas en la Facultad de Ciencias. Ha trabajado en análisis de datos y modelos de machine learning. Actualmente es ayudante de la materia de Lingüística Computacional en la Facultad de Ciencias. Su enfoque profesional se centra en el desarrollo y aplicación de técnicas de aprendizaje automático para resolver problemas aplicados.
</p>
		</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>

<div id="trivia" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
	<h2>Trivia</h2>
  <p>
    Entre más participes 🙋 tratando de responder a las preguntas ❔, más chance de ganar en la tómbola 🤞.
  </p>
		</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>

<div id="platica-langchain" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
	<h2>Construyendo chatbots con LangChain</h2>
	<h3>David Duran, FP-FM-IIMAS, UNAM</h3>
  <p>
En este taller práctico, aprenderás a diseñar e implementar chatbots inteligentes utilizando <strong>LangChain</strong>, un framework para crear aplicaciones con <strong>LLMs</strong>. Exploraremos cómo usar <strong>RAG (Retrieval-Augmented Generation)</strong>, una técnica que permite a la solución integrar nueva información sin reentrenar ni ajustar al LLM. Actualmente existe una tendencia hacia la implementación de soluciones de IA personalizadas, por lo que este tema es de relevancia para el campo laboral.
  </p>
  <p>
<strong>Nota sobre la dinámica:</strong> no es obligatorio contar con equipo de computo durante la sesión. Podrás acceder a la libreta de Colab después del taller, pero también puedes desarrollar tu chatbot durante la sesión.
  </p>
  <h3>Semblanza</h3>
<p>
Actualmente lidera el desarrollo de un chatbot para la Facultad de Medicina de la UNAM. Durante 2024 participó en un proyecto con el IIMAS para la identificación y diseño de una solución para la automatización de tareas en una institución bancaria. Es pasante de la licenciatura en psicología por la UNAM desarrollando una tésis sobre metáforas e inteligencia artificial.
</p>
		</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>

<div id="taller-modelos" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
    <h2>Modelos Ocultos de Márkov</h2></br>
    <h3>Ramón Campos, FC</h3>
  <p>
         Los Modelos Ocultos de Márkov están presentes en muchas áreas de la Ciencia y Tecnología, pero, ¿Que son los Modelos Ocultos de Márkov? En esta presentación se verá y un problema relacionado.
  </p>
  <h3>Semblanza</h3>
<p>
    Estudiante de licenciatura a punto de terminar y aspirante a posgrado, intereses principales en aprendizaje bayesiano y en aprendizaje causal.
</p>
		</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>

<div id="taller-rn" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
    <h2>Temas Básicos de Redes Neuronales</h2></br>
    <h3>José Olmedo</h3>
  <p>
        Este taller está diseñado para quienes desean introducirse en el mundo de la inteligencia artificial y el deep learning, proporcionando una base sólida para seguir explorando temas más avanzados.
  </p>
  <h3>Semblanza</h3>
<p>
Soy José Ángel Olmedo Guevara, egresado en ingeniería en mecatrónica por la UNAM y estudiante de ciencias de la computación en la facultad de ciencias. Realizando mi tesis en el campo del aprendizaje profundo empleando redes neuronales LSTM para la identificación de actividades físicas mediante sensores de movimiento. Cuento con poca experiencia en el campo laboral de la industria privada ya que solía trabajar en el area de bancos, siempre en busca de nuevos retos para seguir creciendo personal y profesionalmente.
</p>
		</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>


<div id="taller-cv" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
    <h2>Redactando mi CV para la industria</h2></br>
    <h3>Axel Castillo Sánchez, Grupo Chamberlain</h3>
  <p>
Guía para definir las secciones más relevantes que deben venir en un currículum y pulir su redacción.
  </p>
  <h3>Semblanza</h3>
<p>
Egresado de la Facultad de Ingeniería de la UNAM y miembro del Lab52+. Llevo 3 años trabajando en la industria de dispositivos IoT para el control de accesos y tengo 2 años de experiencia en reclutamiento de programadores junior. 
</p>
		</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>



<div id="platica-infra" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
	<h2>El intraemprendedor moderno: El trébol de cuatro hojas en la era empresarial</h2>
	<h3>Rogelio Meneses, Founder & CEO Keex Makerlab</h3>
  <p>
Descubre cómo liderar la innovación desde cualquier posición, rompiendo barreras y desafiando el status quo empresarial. Aprende las 7 claves para convertirte en un intraemprendedor bizarro: valiente, creativo y estratégico, maximizando su impacto en tu vida, primer trabajo o proyecto profesional.
  </p>
  <h3>Semblanza</h3>
  <p>
Innovador y líder en la gestión de proyectos de alto impacto con más de 9 años de experiencia en emprendimiento tecnológico, social y de alto impacto. Su propósito es despertar la imaginación de una generación a través de la creación de soluciones disruptivas y sostenibles que impulsen un crecimiento transformador. Experto en el uso de metodologías avanzadas y herramientas para la resolución de problemas, con una sólida trayectoria en la facilitación de talleres, consultoría estratégica y liderazgo en proyectos innovadores.
</p>
		</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>

<div id="taller-trustworthy" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
	<h2>Trustworthy AI: el marco Europeo para el desarrollo responsable de IA</h2>
	<h3>Dra. Sofia Trejo</h3>
  <p>
El taller reflexionará en torno al marco Europeo para el desarrollo responsable de IA, denominado Trustworthy AI. Durante el taller se abordarán:  los procesos involucrados en el desarrollo del marco y su incorporación a la regulación Europea en materia de IA;  los principales elementos del marco, incluyendo principios éticos y requerimientos; limitantes, críticas y futuras lineas de trabajo e investigación.
  </p>
  <h3>Semblanza</h3>
  <p>
  Investigadora especializada en las dimensiones éticas, socio-económicas, políticas y culturales de la IA, con un interés particular en Género y los Sures Globales. Impulsa la incorporación de una reflexión crítica de la tecnología, dentro y fuera de espacios académicos. Participa activamente en la divulgación científica y en el debate público sobre sobre IA. 
</p>
<p>
Es Licenciada en matemáticas por la UNAM y Doctora en Matemáticas por la Universidad de Warwick. Ha trabajado como investigadora en el Barcelona Supercomputing Center, en la Universidad de São Paulo y en Imperial College London.
</p>
		</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>

<div id="platica-genomica" class="modal">
	<div class="modal-background"></div>
		<div class="modal-content">
		<div class="box">
	<h2>Genómica y Computación: Aplicaciones en biomedicina.</h2>
	<h3>Dra. Alejandra Cervera</h3>
  <p>
  En está plática veremos que es la genómica, que clase de datos produce, y como a través de herramientas computacionales podemos darle sentido a los datos para resolver preguntas biomédicas. Nos enfocaremos en un par de aplicaciones concretas de análisis de datos de expresión génica para mejorar el diagnóstico y el entendimiento de un tipo de cáncer y de una enfermedad del corazón.
  </p>
  <h3>Semblanza</h3>
  <p>
        Pronto
</p>
		</div>
		</div>
		<button class="modal-close is-large"
				aria-label="close"></button>
</div>





<script>
document.addEventListener('DOMContentLoaded', () => {
  // Functions to open and close a modal
  function openModal($el) {
    $el.classList.add('is-active');
  }

  function closeModal($el) {
    $el.classList.remove('is-active');
  }

  function closeAllModals() {
    (document.querySelectorAll('.modal') || []).forEach(($modal) => {
      closeModal($modal);
    });
  }

  // Add a click event on buttons to open a specific modal
  (document.querySelectorAll('.js-modal-trigger') || []).forEach(($trigger) => {
    const modal = $trigger.dataset.target;
    const $target = document.getElementById(modal);

    $trigger.addEventListener('click', () => {
      openModal($target);
    });
  });

  // Add a click event on various child elements to close the parent modal
  (document.querySelectorAll('.modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button') || []).forEach(($close) => {
    const $target = $close.closest('.modal');

    $close.addEventListener('click', () => {
      closeModal($target);
    });
  });

  // Add a keyboard event to close all modals
  document.addEventListener('keydown', (event) => {
    if(event.key === "Escape") {
      closeAllModals();
    }
  });
});
</script>
