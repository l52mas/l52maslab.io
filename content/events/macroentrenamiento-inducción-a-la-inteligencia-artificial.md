---
name: "Módulo de Inducción a la Inteligencia Artificial - MeIA"
date: 2023-06-13
happening: 2023-06-13
status: finish
draft: false
---

Los siguientes videos comprenden el módulo de Inducción a la IA que se
impartió como parte del [Macro entrenamiento en Inteligencia
Articicial](https://www.taller-tic.redmacro.unam.mx/MeIA/):


| Sesión| Título | Link video  | Link presentación |
|---    |---     |--- |--- |
| 1     | Matemáticas para la IA                   | [🎞️](https://www.youtube.com/watch?v=CDYLHa63ws4&t=3s)  | [🖵](https://docs.google.com/presentation/d/1jlEEwgZwUuKXQnRJeRCX2ln9WvX56dxmWGuvlJQ575A/edit?usp=sharing) |
| 2     | Programación en python                   | [🎞️](https://www.youtube.com/watch?v=9AwJrXAz9QA&t=1s)  | [🖵](https://docs.google.com/presentation/d/1l2u98QbcQgjyIpisDalILYV4dsc3E_I-gzSsrqpz0tE/edit?usp=sharing) |
| 3     | Introducción a los sistemas inteligentes | [🎞️](https://www.youtube.com/watch?v=ERYgaGKaHoE)  | [🖵](https://docs.google.com/presentation/d/1hLmVtld0zznnOhxIXDqb7UZlWGjcJrOr8s-uBA7iEDA/edit?usp=sharing) |
| 4     | Aprendizaje profundo                     | [🎞️](https://www.youtube.com/watch?v=KX4DdZeRAsI)  | [🖵](https://docs.google.com/presentation/d/1uKBgTRue-NS8FCwKMjsfZ4O651HTw3X8Nwfq7HunPhQ/edit?usp=sharing) |
| 5     | Aprendizaje automático                   | [🎞️](https://www.youtube.com/watch?v=zdpDR_F2ovg) y [🎞️](https://www.youtube.com/watch?v=eAmFytbeNTc&t=2s)  | [🖵](https://docs.google.com/presentation/d/1Jy69MOJltDGwMyFbjCvxKz_X46ewBTASk-Fr6wAIpwg/edit?usp=sharing) |

Con participaciones de:

* Dr. Juan Irving Vazquez (sesión: _Apredizaje profundo_ con plática
    "Convolutional Neural Network")
* Dra. Maria Guadalupe Venteño (sesión: _Aprendizaja automático_ con plática
    "Aplicaciones de IA en la Educaci{on")
