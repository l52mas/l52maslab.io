---
name: "Conversatorio: Prácticas de documentación de la lengua desde lingüística y las tecnologías de
habla"
date: 2024-04-17
happening: 2024-04-22
status: finish
---

## Fecha y lugar

* **Fecha**: 22 de abril, de 12 a 14 horas
* **Presencial**: Presencial, Sala de Juntas, DCC, IIMAS
* **Transmisión en línea**:  https://www.youtube.com/@laboratoriol52mas

## Resumen

En este conversatorio abordaremos las prácticas de la documentación de la lengua
desde los objetivos complementarios que tienen la Lingüística y las tecnologías
del habla. El objetivo es crear unas notas cortas sobre prácticas deseables
basadas en las consideraciones que ambas áreas aplican durante la práctica.

### Preguntas detonantes

* ¿Cómo se identifican a los hablantes?
* ¿Qué elementos de consideran durante las entrevistas lingüísticas?
* ¿Cómo se registran los datos lingüísticos?
* ¿Prácticas de construcción de corpus?
* ¿El rol del habla como registro lingüístico?
* ¿Consideraciones al compartir corpus?



## Invitado especial

* Dr. Manuel Alejandro Sánchez Fernandez</br>
  [Universidad Autónoma de Baja California](https://www.bsc.es/)</br>


## Participantes

* Mtro. Erik Ramos Aguilar</br>
  [UPIIT, IPN](https://www.upiit.ipn.mx/)</br>

* Dr. Ricardo Ramos Aguilar</br>
  [UPIIT, IPN](https://www.upiit.ipn.mx/)</br>

* Jesús Priego
  FC e IIMAS, UNAM

* David Duran
  FP e IIMAS, UNAM

* Dr. Ivan Vladimir Meza Ruiz</br>
  [IIMAS, UNAM](https://www.iimas.unam.mx/)</br>

## Videos


<iframe width="560" height="315" src="https://www.youtube.com/embed/uUQ5fV5DJE8?si=Du1_sd1A9S19pMJy&amp;start=759" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


## Confirmar asistencia enviando correo a:

[**ivanvladimir+seminario@gmail.com**](mailto:ivanvladimir+seminario@gmail.com)








