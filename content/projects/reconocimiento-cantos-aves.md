---
title: "Reconocimeinto de cantos de aves"
date: 2022-01-13T11:16:47-06:00
---

Este proyecto busca hacer sistemas inteligentes que reconozcan diferentes aspectos de los cantos de aves.

---

## Model cards

La siguiente es una descripción del tipo de sistemas que se están siguiendo 

<table class="table">
  <thead>
    <tr>
      <th>Aspectos</th>
      <th>Descripción</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Detalles del modelo</td>
      <td>
        Programado por <strong>personas</strong> del Instituto de Investigaciones en Matemáticas Aplicadas y en Sistemas del laboratorio L52+
      </td>
    </tr>
 <tr>
  <td>Uso previsto</td>
      <td>
        <ul>
        <li>Detección y clasificación del canto de diferentes especies de aves utilizando IA.
 </li>
        <li>Con los datos obtenidos identificar posibles deterioros ambientales o información sobre la biodiversidad en distintas regiones.
 </li>
        <li> Hacer análisis ecológicos de manera más rápida con ayuda de IA </li>
            </ul>
      </td>
    </tr>
  <tr>
 <td>Factores</td>
      <td>
        <ul>
        <li> Examinación de bases de datos acústicas de paisajes naturales para la identificación y clasificación de señales específicas (en este caso, cantos de aves)

 </li>
        <li> Los datos originales vienen del sitio Xeno-canto.
 </li>
        <li> Base de datos utilizada por la competencia </li>
         <li> Los datos son transformados a espectrogramas </li>
            </ul>
      </td>
    </tr>
 <tr>
 <td>Training data</td>
      <td>
        <ul>
        <li> 14,100 grabaciones de más de 501 especies de aves provenientes de América del norte, Centroamérica y Sudamérica.

 </li>
        <li> No son datos balanceados.
 </li>
            </ul>
      </td>
    </tr>
     <tr>
 <td>Datos de evaluación</td>
      <td>
        <ul>
       Un corte de los datos de la base de datos utilizada.
 </ul>
      </td>
    </tr>
     <tr>
 <td>Métrica</td>
      <td>
        <ul>
       Precisión promedio (mean Average Precision, MAP)
 </ul>
      </td>
    </tr>
     <tr>
 <td>Consideraciones éticas</td>
      <td>
        <ul>
        El uso del sistema está considerado en un sistema ecológico y se deben tomar en cuenta los factores de éste y las opiniones de expertos sobre:
        <li> La composición de los paisajes sonoros

 </li>
        <li> Las consecuencias de las intervenciones
 </li>
        <li> La interpretación de resultados  </li>
         <li> ... y otras. </li>
            </ul>
      </td>
    </tr>
    <tr>
 <td>Recomendaciones</td>
      <td>
        <ul>
        <li> Tomar en cuenta aspectos y conceptos tanto ecológicos como biológicos 

 </li>
        <li> Analizar la vida útil  a largo o corto plazo de los datos.
 </li>
            </ul>
      </td>
    </tr>
  </tbody>
</table>

### Referencias

1. Mitchell, M., Wu, S., Zaldivar, A., Barnes, P., Vasserman, L., Hutchinson, B., ... & Gebru, T. (2019, January). Model cards for model reporting. In Proceedings of the conference on fairness, accountability, and transparency (pp. 220-229).
2. Gebru, T., Morgenstern, J., Vecchione, B., Vaughan, J. W., Wallach, H., Iii, H. D., & Crawford, K. (2021). Datasheets for datasets. Communications of the ACM, 64(12), 86-92.
