---
title: "Laboratorio L52+"
description: Laboratorio en Lenguaje Humano🗨️ e Inteligencia Artificial 🤖
date: 2019-01-21T16:18:23-06:00
---

Trabajamos en *dar el don del lenguaje a las computadoras* para procesar y
entender grandes cantidades de textos, para que las computadoras interactúen con
humanos o para que estas tengan comportamientos inteligentes. Nos especializamos en varias técnicas:

* [_Aprendizaje profundo_](https://es.wikipedia.org/wiki/Aprendizaje_profundo)
    (_deep learning_)
* [_Aprendizaje
    automático_](https://es.wikipedia.org/wiki/Aprendizaje_autom%C3%A1tico)
    (_machine learning_)
* [_Ciencia de datos_](https://es.wikipedia.org/wiki/Ciencia_de_datos) (_data
    science_)
* [_Inteligencia
    artificial_](https://es.wikipedia.org/wiki/Inteligencia_artificial)
    (_artificial intelligence_)

Nuestro investigación nos lleva indagar cómo aplicar lo que sabemos del lenguaje
e inteligencia artificial a otras áreas como la Biología, el Derecho o las Humanidades
Digitales.

Algunas de las áreas que trabajamos son:

* [Lenguas originarias de México]({{< ref "areas/mexican-indigenous-languages.md" >}})
* [Agentes conversacionales]({{< ref "areas/conversational-agents.md" >}})
* [Aprendizaje automático]({{< ref "areas/machine-learning.md" >}})
* [Voz forense]({{< ref "areas/forensic-speech.md" >}})
* [Reconocimiento voz]({{< ref "areas/speech.md" >}})
* [Aspectos meta-lingüísticos]({{< ref "areas/metalinguistic.md">}})
* [Aplicaciones]({{< ref "areas/applications.md" >}})

Para aprender más como participar en el laboratorio puedes leer sobre las
[opciones](participation) y nuestra sección de [preguntas frequentes](faq).


Si quieres saber más sobre nosotros, los proyectos o tienes una idea en la que 
creas que te podemos apoyar, no dudes en [cotactarnos]({{< relref "contact"
>}}).
