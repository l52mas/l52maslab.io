---
title: "Unirte al Laboratorio"
date: 2021-03-31T20:14:30-06:00
draft: false
---

Si quieres participar en el laboratorio revisa las [áreas]({{< ref "areas" >}})
y [proyectos]({{< ref "projects" >}}), y si
alguno te llama la atención lee más sobre este y revisa las formas en que puedes
participar. Para unirte al laboratorio estas
son algunas de las modalidades con las que tenemos experiencia:

* [Servicio social](#servicio-social)
* [Desarrollo de tesis](#tesis)
* [Residencia/Prácticas](#residenciapracticas)
* [Veranos de investigación](#verano-de-investigación)
* [Estancias de investigación](#estancias-de-investigación)

#### Requisitos generales

No existe como tal requisitos de nuestra parte, pero generalmente los procesos
son algo extensos y mucho del éxito de estos es tener la **paciencia**. Para
tener una experiencia en nuestro laboratorio es recomendable:

1. Saber **programar**, o si el área de la que provienes no es de cómputo, tener
   ganas de aprender a programar.
2. Ser una/un estudiante **autónoma/o**, es decir poder estudiar por tu cuenta e
   intentar resolver los problemas por tu lado.
3. Trabajar bien en **equipo** la mayoría de nuestros proyectos son en
   colaboración con grupos de estudiantes.


## Servicio social 

Es posible hacer el servicio social con nosotros bajo el programa con nombre
"Desarrollo de sistemas inteligentes usando _deep learning_" con clave
_2021-12/53-19_. 

En los últimos meses hemos tenido mucha demanda con el programa por lo que no
podemos registrar estudiantes en cualquier momento. Antes de comenzar a
registrar estudiantes organizamos sesiones informativas, registra tus datos para
enterarte de la siguiente (generalmente en los periodos intersemestrales)

<center>
<a
href="https://docs.google.com/forms/d/e/1FAIpQLSeoJto8svHnVnCIuCm2F0vAzAchjN3neloCo0fSn9kQ3DdYjQ/viewform?usp=sf_link"
class="button is-info">Sesión informativa servicio social</a>
</center>

#### Requisitos 


* Cumplir con la cantidad de créditos especificada por la facultad.
* Tener disponibilidad de tiempo para llevar a cabo el servicio social.
* Revisar el procedimiento con la facultad.
* Identificar una de las áreas o proyectos a los cuales quieras apoyar durante
    tu servicio.

## Tesis

Si estás buscando desarrollar una tesis, también es posible realizarla en
nuestro laboratorio. Algunas consideraciones:

1. El mínimo de tiempo para desarrollar una tesis son _6_ meses a partir de
   tener la propuesta, si se le dedican 4 horas diarias a esta, y al final de
   esos meses se puede aspirar a tener un borrador completo de la tesis.
2. Identificar el tema es un proceso que puede tomar de _1_ a _2_ meses.
3. Una vez con el borrador completo, el proceso de titulación puede tomar de _2_
   a _4_ meses.
4. Estos tiempos son aproximados, el secreto para cumplir con los tiempos 1 y 2,
   es dedicación al desarrollo de la tesis.
5. Cuando se trabaja y se desarrolla una tesis, los tiempos tienden a duplicarse
   o triplicase. 

#### Requisitos

1. Tener interés en las [áreas]({{< ref "areas" >}})
y [proyectos]({{< ref "projects" >}}) desarrollados en el laboratorio o
   tener una idea en sincronía con estos.
2. Tener disponibilidad de tiempo para desarrollar la tesis.

## Residencia/Prácticas

Es posible realizar residencia o prácticas o estancias largas e nuestro
laboratorio, sólo es importante cumplir con todas los procesos que tu  
universidad ponga y asegurar que nuestro laboratorio cumple con las expectativas
de la experiencia esperada.

#### Requisitos

* Verificar que hacer un proyecto de investigación sea una opción válida para
    la residencia, práctica o estancia larga.
* Verificar que la UNAM e IIMAS cumplan con la especificación de su universidad.
* Identificar un proyecto a desarrollar dentro de nuestras áreas de
    investigación.

## Veranos de investigación

Nuestro laboratorio participa en los programas de [Programa
Delfín](https://www.programadelfin.org.mx) y [Verano de la Investigación
Científica](https://www.amc.edu.mx/amc/index.php?option=com_content&view=article&id=139&Itemid=324)
en los que puedes pasar de 7 a 8 semanas trabajando en un proyecto durante el
verano. 

#### Requisitos

* Seguir el procedimiento de la convocatoria

## Estancias de investigación

Estancia de investigación es como llamamos cuando un/una estudiante dedica parte
de su tiempo libre a apoyar uno de nuestros proyectos. De alguna forma es una
colaboración "informal" entre el estudiante y nuestro laboratorio.

#### Requisitos

* Identificar proyecto a colaborar
* Compromiso de un número determinado de horas que se dedicarán al proyecto.

